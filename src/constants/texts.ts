export const HELP = {
  settings : {
    title : "Editez vos messages personnalisés",
    content : "Ici, vous pouvez ajouter différents messages personnalisés pour tous vos contacts ! Ainsi, lorsqu'un interlocuteur vous appelera au cours d'un trajet, il recevra le message personnalisé qui lui correspond. Si aucun message personnalisé ne lui a été attribué, il recevra alors le message par défaut (également personnalisable)."
  }
}