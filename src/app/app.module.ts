import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { NowIDriveApp } from './app.component';

//Modals
import { HelpModal } from '../modals/help/help';
import { JourneyModal } from '../modals/journey/journey';
import { LogsModal } from '../modals/logs/logs';

//Native plugins
import { Contacts } from '@ionic-native/contacts';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { SMS } from '@ionic-native/sms';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

//Pages
import { AccountPage } from '../pages/account/account';
import { HomePage } from '../pages/home/home';
import { SettingsPage } from '../pages/settings/settings';

//Pipes
import { NiceNumber } from '../pipes/nice-number';
import { NiceTime } from '../pipes/nice-time';

//Services
import { ApiService } from '../services/api.service'

@NgModule({
  declarations: [
    NowIDriveApp,
    //Modals
    HelpModal,
    JourneyModal,
    LogsModal,
    //Pages
    AccountPage,
    HomePage,
    SettingsPage,
    //Pipes
    NiceNumber,
    NiceTime
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(NowIDriveApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    NowIDriveApp,
    //Modals
    HelpModal,
    JourneyModal,
    LogsModal,
    //Pages
    AccountPage,
    HomePage,
    SettingsPage
  ],
  providers: [
    //Native plugins
    Contacts,
    Geolocation,
    LocationAccuracy,
    SMS,
    SocialSharing,
    SplashScreen,
    StatusBar,
    //Pipes
    NiceNumber,
    NiceTime,
    //Services
    ApiService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
