import { Component, ViewChild } from '@angular/core';
import { ModalController, Platform, Nav } from 'ionic-angular';

//Native plugins
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//Modals
import { LogsModal } from '../modals/logs/logs';

//Pages
import { AccountPage } from '../pages/account/account';
import { HomePage } from '../pages/home/home';
import { SettingsPage } from '../pages/settings/settings';

@Component({
  templateUrl: 'app.html'
})
export class NowIDriveApp {
  @ViewChild(Nav) nav: Nav;

  constructor(
    private platform: Platform,
    private modalCtrl : ModalController,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen
  ) {
    this.initApp();
  }

  initApp(){
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.nav.setRoot(HomePage);
    });
  }

  goToAccount(){
    this.nav.setRoot(AccountPage);
  }
  
  goToHome(){
    this.nav.setRoot(HomePage);
  }

  goToSettings(){
    this.nav.setRoot(SettingsPage);
  }

  openLogs(){
    let modalLogs = this.modalCtrl.create(LogsModal);
    modalLogs.present();
  }

}
