export const CONSTANTS_API = {
  CAUSE_WENT_BACKGROUND : "went_background",
  CAUSE_ANSWERED_CALL : "answered_wall",
  CAUSE_END_JOURNEY : "end_journey",
  CAUSE_MESSAGE : "message",
  CAUSE_NO_BATTERY : "no_battery",
  CAUSE_UNKNOWN    : "unknown"
}

