import { Component, OnInit } from '@angular/core';
import { Platform, AlertController, ToastController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

//Constants
import { CONSTANTS_API } from '../../app/app.constants';
import { GLOBAL} from '../../constants/global';

//Native Plugins
import { Geolocation } from '@ionic-native/geolocation';
import { SMS as SMS_Sender }from '@ionic-native/sms';

//Services
import { ApiService } from '../../services/api.service';

//Plugin to trap a call : https://github.com/renanoliveira/cordova-phone-call-trap
//Doc for the SMS plugin : https://github.com/floatinghotpot/cordova-plugin-sms
//Doc for integrating Cordova plugins into Ionic 2 : https://www.joshmorony.com/using-cordova-plugins-in-ionic-2-with-ionic-native/
declare var SMS;
declare var PhoneCallTrap;

@Component({
  selector: "modal-journey",
  templateUrl: 'journey.html'
})
export class JourneyModal implements OnInit {
  
  //Variables
  protected receivedCalls     : any[];
  protected receivedMessages  : any[];
  protected contactMessages   : any[];
  protected defaultMessage    : string;
  protected elapsedTime       : number;
  protected intervalElapse    : any;
  protected positions         : any;
  protected journeyStarted    : boolean;
  protected alertLoadingLoc   : any;

  constructor(
    private platform : Platform,
    private alertCtrl : AlertController,
    private toastCtrl : ToastController,
    private viewCtrl : ViewController,
    private storage : Storage,
    private geolocation : Geolocation,
    private sms : SMS_Sender,
    private _apiService : ApiService
  ) {
    this.receivedCalls = [];
    this.receivedMessages = [];
    this.contactMessages = [];
    this.defaultMessage = "";
    this.elapsedTime = 0;
    this.intervalElapse = null;
    this.positions = {
      initial : null,
      final : null
    };
    this.journeyStarted = false;
    this.alertLoadingLoc   = null;
  }

  ngOnInit(){
    this.initLoadingLocationAlert();
    this.getDefaultMessageFromCache();
    this.getContactMessagesFromCache();
    this.platform.pause.subscribe(() => { //Emitted when the app goes background
      console.warn("The app went background");
      this.stopJourney(CONSTANTS_API.CAUSE_WENT_BACKGROUND);
    });
    this.watchCalls();
  }

  //------------------------------------------------------------------//
  //----------------------------JOURNEY-------------------------------//
  //------------------------------------------------------------------//

  startJourney(){
    this.journeyStarted = true;
    this.alertLoadingLoc.present();
    this.setPosition("initial").then(() => {
      if(!this.journeyStarted){
        return;
      }
      this.alertLoadingLoc.dismiss();
      this.initLoadingLocationAlert();

      this.watchSMS();
      this.intervalElapse = setInterval(() => {
        this.elapsedTime += 1000;
      }, 1000);
      
    });
  }

  stopJourney(cause = CONSTANTS_API.CAUSE_END_JOURNEY){
    if(!this.journeyStarted){
      return;
    }

    this.journeyStarted = false;
    this.unwatchSMS();
    clearInterval(this.intervalElapse);
    this.receivedMessages = [];
    this.receivedCalls    = [];

    this.setPosition("final").then(() => {
      this.sendJourney(cause);
    });
  }

  setPosition(moment : string){
    return this.geolocation.getCurrentPosition().then(data => {
      let coords = {
        latitude : data.coords.latitude,
        longitude : data.coords.longitude
      }
      switch(moment){
        case "initial":
          this.positions.initial = coords;
          break;
        case "final":
          this.positions.final = coords;
          break;
      }
      console.log("GEOLOCATION : storing "+moment+" location : ", this.positions);
      
    }, error => {
      console.error("GEOLOCATION (error) : ",error);
    });
  }

  sendJourney(cause : string){
    let totalDistance = this.computeDistance();
    let totalTime = Math.floor(this.elapsedTime / 1000); //total time in seconds

    let journey = {
      date : new Date(),
      cause : cause,
      distance : totalDistance, //in km
      duration : totalTime,  //in seconds
      positions : this.positions
    };
    this.displaySummaryJourneyAlert(journey);
    if(cause == CONSTANTS_API.CAUSE_END_JOURNEY){
      this.updateScoreInCache(journey);
    }
    this.storeJourneyIntoCache(journey);

    this.positions = {
      initial : null,
      final : null
    };
    this.elapsedTime = 0;
  }

  //------------------------------------------------------------------//
  //-------------------------CACHE FUNCTION---------------------------//
  //------------------------------------------------------------------//

  storeJourneyIntoCache(journey : any){
    this.storage.get(GLOBAL.appName+"_journeys").then(journeys => {
      if(journeys == null){
        journeys = [journey];
      }else{
        journeys.push(journey);
      }
      console.log("JOURNEY : stored in cache - ", journeys);
      this.storage.set(GLOBAL.appName+"_journeys", journeys);
    });
  }

  updateScoreInCache(journey : any){
    this.storage.get(GLOBAL.appName+"_distanceScore").then(distanceScore => {
      if(distanceScore == null){
        distanceScore = journey.distance;
      }else{
        distanceScore += journey.distance;
      }
      this.storage.set(GLOBAL.appName+"_distanceScore", distanceScore);
      console.log("CACHE : distanceScore ", distanceScore);
    });
  }

  getContactMessagesFromCache(){
    this.storage.get(GLOBAL.appName+"_contactMessages").then(contactMessages => {
      if(contactMessages != null && contactMessages.length > 0){ //If some contactMessages are stored in the cache
        this.contactMessages = contactMessages;
      }else{
        this.contactMessages = [];
        this.storage.set(GLOBAL.appName+"_contactMessages", []);
      }
      console.log("CONTACTMESSAGES : ", this.contactMessages);
    });
  }

  getDefaultMessageFromCache(){
    this.storage.get(GLOBAL.appName+"_defaultMessage").then(defaultMessage => {
      if(defaultMessage == null){
        this.defaultMessage = "";
        this.storage.set(GLOBAL.appName+"_defaultMessage", "");
      }else{
        this.defaultMessage = defaultMessage;
      }
      console.log("CONTACTMESSAGES : defaultMessage ", this.defaultMessage);
    })
  }

  //------------------------------------------------------------------//
  //----------------------------CALLS & SMS---------------------------//
  //------------------------------------------------------------------//

  hideNotif(indexNotif : number, type = "message"){
    switch(type){
      case "message":
        this.receivedMessages.splice(indexNotif,1);
        return;
      case "call":
        this.receivedCalls.splice(indexNotif,1);
        return;
    }
  }

  setAnsweredNotif(indexNotif : number, type = "message"){
    switch(type){
      case "message":
        this.receivedMessages[indexNotif].answered = true;
        return;
      case "call":
        this.receivedCalls[indexNotif].answered = true;
        return;
    }
  }

  sendMessage(indexNotif : number, type = "message"){
    let numeroToAnswer = "-1";
    switch(type){
      case "call":
        numeroToAnswer = this.receivedCalls[indexNotif].caller;
        break;
      case "message":
        numeroToAnswer = this.receivedMessages[indexNotif].sender;
        break;
    }
    this.storage.get(GLOBAL.appName+"_contactMessages").then(contactMessages => {
      let indexContact = -1;
      for(let i = 0, l = contactMessages.length ; i < l ; i++){
        let contactMessage = contactMessages[i];
        for(let j = 0, m = contactMessage.contact.numeros.length ; j < m ; j++ ){
          if(contactMessage.contact.numeros[j] == numeroToAnswer){
            indexContact = i;
            break;
          }
        }
      }

      if(indexContact != -1){ //if the contact has been found
        this.sms.send(numeroToAnswer, contactMessages[indexContact].message);

        this.displayToast("Le SMS personnalisé à "+contactMessages[indexContact].contact.name+" a bien été envoyé");
      }else if(this.defaultMessage != ""){
        this.sms.send(numeroToAnswer, this.defaultMessage);
        this.displayToast("Pas de SMS personnalisé pour "+ numeroToAnswer +" : le SMS par défault a été envoyé");
      }else{
        this.displayToast("Vous n'avez pas spécifié de message par défaut");
      }
    });
  }

  watchSMS(){
    if(!this.platform.is("cordova")){
      console.log("SMS : This platform does not support Cordova.");
      return;
    }
    if(SMS){
      SMS.startWatch( success => {
        document.addEventListener('onSMSArrive',  this.readSMS.bind(this));
      }, err => {
        console.error("Error while calling startWatch method : ",err);
      });
    }else{
      console.error("Error : SMS object doesn't exist");
    }
  }

  readSMS(e : any){
    let sms = e.data;
    console.log("SMS : SMS received ", sms);

    let receivedMessage = {
      sender : sms.address,
      time: sms.date,
      content : sms.body,
      answered : false
    }
    this.receivedMessages.push(receivedMessage);
  }

  unwatchSMS(){
    if(!this.platform.is("cordova")){
      return;
    }
    if(SMS){
      SMS.stopWatch( success => {
        document.removeEventListener('onSMSArrive', this.readSMS);
      }, err => {
        console.error("Error while calling stopWatch method : ",err);
      });
    }else{
      console.error("Error : SMS object doesn't exist");
    }
  }

  watchCalls(){
    if(!this.platform.is("cordova")){
      console.log("CALLS : This platform does not support Cordova.");
      return;
    }
    if(PhoneCallTrap){
      PhoneCallTrap.onCall(this.handleChangePhoneState.bind(this)); //To handle incoming calls
    }else{
      console.error("CALL ERROR : PhoneCallTrap object doesn't exist");
    }
  }

  handleChangePhoneState(state : string){
    if(!this.journeyStarted){
      return;
    }

    if(state == "OFFHOOK"){
      console.log("PHONE : Phone is off-hook");
      this.stopJourney(CONSTANTS_API.CAUSE_ANSWERED_CALL);
    }else if(state == "IDLE"){
      console.log("PHONE : Phone is idle");
    }else if(state.substr(0,7) == "RINGING"){
      console.log("PHONE : Phone is ringing, incoming call number '"+state.substr(8)+"'");
      let receivedCall = {
        caller : state.substr(8),
        time : Date.now(),
        answered : false
      }
      this.receivedCalls.push(receivedCall);
    }else{
      console.warn("PHONE : could not handle this event, state : ",state);
    }

  }

  //------------------------------------------------------------------//
  //-----------------------TOAST, ALERT & MODALS----------------------//
  //------------------------------------------------------------------//

  displayLoadingLocationAlert(){
    
  }

  initLoadingLocationAlert(){
    this.alertLoadingLoc = this.alertCtrl.create({enableBackdropDismiss: false});
    this.alertLoadingLoc.setTitle("Géolocalisation");
    this.alertLoadingLoc.setMessage("Accès aux données GPS en cours, veuillez patienter...");
    this.alertLoadingLoc.addButton({
      text: "Annuler",
      handler: () => {
        this.journeyStarted = false;
        return false;
      }
    });
  }

  dismissLoadingLocationAlert(){

  }

  displaySummaryJourneyAlert(journey : any){
    let alertSummary = this.alertCtrl.create();
    alertSummary.setTitle("Résumé du voyage");
    alertSummary.setMessage(
      "Durée : "+ Math.floor(journey.duration)+"s \n "+
      "Distance : "+ Math.floor(journey.distance) + "km");
    alertSummary.present();
  }

  displayToast(message : string, duration = 2000, position = "bottom"){
    let options = {
      message : message,
      duration : duration,
      position : position
    }
    let toast = this.toastCtrl.create(options);
    toast.present();
  }

  closeModal(){
    if(this.journeyStarted){
      this.stopJourney(CONSTANTS_API.CAUSE_WENT_BACKGROUND);
    }
    this.viewCtrl.dismiss();
  }

  //------------------------------------------------------------------//
  //----------------------------COMPUTATION---------------------------//
  //------------------------------------------------------------------// 

  computeDistance() {
    if(this.positions.initial.latitude == null
      || this.positions.initial.longitude == null
      || this.positions.final.latitude == null
      || this.positions.final.longitude == null){
      console.error("COMPUTATION ERROR : this.positions contains a null value : ", this.positions);
      return;
    }
    let radLat1 = Math.PI * this.positions.initial.latitude/180;
    let radLat2 = Math.PI * this.positions.final.latitude/180;
    let theta = this.positions.initial.longitude-this.positions.final.longitude;
    let radTheta = Math.PI * theta/180;
    let distance = Math.sin(radLat1) * Math.sin(radLat2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.cos(radTheta);
    distance = Math.acos(distance);
    distance = distance * 180/Math.PI;
    distance = distance * 60 * 1.1515;
    distance = distance * 1.609344;
    return distance;
}


}
