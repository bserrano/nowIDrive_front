import { Component, OnInit } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Geolocation } from '@ionic-native/geolocation';

//Constants
import { GLOBAL} from '../../constants/global';

@Component({
  selector: "modal-logs",
  templateUrl: 'logs.html'
})
export class LogsModal implements OnInit {
  protected journeys : any[];
  protected contactMessages : any[];
  protected valueTest = false;

  constructor(
    private viewCtrl : ViewController,
    private storage : Storage,
    private geolocation : Geolocation,
    private locationAccuracy : LocationAccuracy
  ) {
  }


  ngOnInit(){
    this.getValuesFromStorage();
    this.test();
  }

  getValuesFromStorage(){
    this.storage.get(GLOBAL.appName + "_journeys").then(journeys => {
      console.log("CACHE : journeys - ", journeys);
      this.journeys = journeys;
    });
    this.storage.get(GLOBAL.appName + "_contactMessages").then(contactMessages => {
      console.log("CACHE : contactMessages - ", contactMessages);
      this.contactMessages = contactMessages;
    });
  }

  test(){
    console.log("CALLING test function");
  }

  closeModal(){
    this.viewCtrl.dismiss();
  }

}
