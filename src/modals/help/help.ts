import { Component, OnInit } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

//Constants
import { GLOBAL } from '../../constants/global';
import { HELP } from '../../constants/texts';

@Component({
  selector: 'modal-help',
  templateUrl: 'help.html'
})
export class HelpModal implements OnInit {
  //Constants
  protected text : any;

  //Parameters
  protected key     : string;

  constructor(
    private storage : Storage,
    private navParams : NavParams,
    private viewCtrl : ViewController
  ) {
    this.key = navParams.get("key");
  }

  ngOnInit(){
    for(let key in HELP){
      if(key == this.key){
        this.text = HELP[key];
        return;
      }
    }

    this.text = {title : "No title", content : "No content"};
  }


  closeModal(){
    this.storage.set(GLOBAL.appName+"_help_"+this.key, true).then(() => {
      this.viewCtrl.dismiss();
    });
  }



}
