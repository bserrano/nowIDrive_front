import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { GLOBAL } from '../constants/global';

//Services
// import { WSSEService } from './wsse.service';

@Injectable()

export class ApiService{

  constructor(
    public http : Http
    // public _wsseService : WSSEService
  ){}

  /**
  * Send the info about a journey to the API.
  * param: journey -
  * cause : see CONSTANTS_API
  * distance : in kilometers
  * duration : in seconds
  */
  sendJourney(journey : any)
  {
    let body = {
      cause : journey.cause,
      distance : journey.distance,
      duration : journey.duration
    };

    return this.http.post(GLOBAL.urlAPI + "/journey/send", body);
  }

}
