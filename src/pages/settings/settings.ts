import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController, ModalController } from 'ionic-angular';

//Constants
import { GLOBAL } from '../../constants/global';

//Modals
import { HelpModal } from '../../modals/help/help';

//Plugins
import { Contacts } from '@ionic-native/contacts';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  protected contactMessages : any[];
  protected defaultMessage  : string;

  constructor(
    private contacts : Contacts,
    private storage : Storage,
    private alertCtrl : AlertController,
    private modalCtrl : ModalController
  ) {
    this.contactMessages = [];
    this.defaultMessage = "";
  }

  ngOnInit(){
    this.displayHelpModal();
    this.getContactMessagesFromCache();
    this.getDefaultMessageFromCache();
  }

  //------------------------------------------------------------------//
  //----------------------------CACHE---------------------------------//
  //------------------------------------------------------------------//

  getContactMessagesFromCache(){
    this.storage.get(GLOBAL.appName+"_contactMessages").then(contactMessages => {
      if(contactMessages != null && contactMessages.length > 0){ //If some contactMessages are stored in the cache
        this.contactMessages = contactMessages;
      }else{
        this.contactMessages = [];
        this.storage.set(GLOBAL.appName+"_contactMessages", []);
      }
      console.log("CONTACTMESSAGES : ", this.contactMessages);
    });
  }

  getDefaultMessageFromCache(){
    this.storage.get(GLOBAL.appName+"_defaultMessage").then(defaultMessage => {
      if(defaultMessage == null){
        this.defaultMessage = "";
        this.storage.set(GLOBAL.appName+"_defaultMessage", "");
      }else{
        this.defaultMessage = defaultMessage;
      }
      console.log("CONTACTMESSAGES : defaultMessage ", this.defaultMessage);
    })
  }

  storeContactMessageIntoCache(contact : any, message : string){
    this.storage.get(GLOBAL.appName+"_contactMessages").then(contactMessages => {
      contactMessages.push({
        contact : contact,
        message : message
      });
      this.contactMessages = contactMessages;
      this.storage.set(GLOBAL.appName+"_contactMessages",contactMessages);
      console.log("CONTACTMESSAGES : ", this.contactMessages);
    });
  }

  updateContactMessageInCache(indexContactMessage : number, message : string){
    if(indexContactMessage == -1){ //If you want to update the default message
      this.defaultMessage = message;
      this.storage.set(GLOBAL.appName+"_defaultMessage", message);
      console.log("CONTACTMESSAGE : defaultMessage ",message);
    }else{
      this.storage.get(GLOBAL.appName+"_contactMessages").then(contactMessages => {
        contactMessages[indexContactMessage].message = message;
        this.contactMessages = contactMessages;
        this.storage.set(GLOBAL.appName+"_contactMessages",contactMessages);
        console.log("CONTACTMESSAGES : ", this.contactMessages);

      });
    }
  }

  //------------------------------------------------------------------//
  //-----------------------MODAL & ALERT------------------------------//
  //------------------------------------------------------------------//

  displayHelpModal(force = false){
    let key = "settings";
    this.storage.get(GLOBAL.appName + "_help_"+key).then(already => {
      if(force || !already){  //If the help modal has never been displayed
        let modalHelp = this.modalCtrl.create(HelpModal, {key: key});
        modalHelp.present();
      }
    });
  }

  displayAddContactMessageAlert(){
    //Open the contact to select the one you want to send a message to
    this.contacts.pickContact().then(contactPicked => {
      console.log("Contact picked : ", contactPicked);
      let phoneNumbers = [];
      for(let i = 0, l = contactPicked.phoneNumbers.length ; i < l ; i++){
        phoneNumbers.push(contactPicked.phoneNumbers[i].value);
      }
      let contact = {
        name : contactPicked.displayName,
        numeros : phoneNumbers
      }

      let alertInputMessage = this.alertCtrl.create({
        title: "Attribuer un message",
        message: "Tape le message que tu souhaites renvoyer automatiquement à "+ contact.name
      });
      alertInputMessage.addInput({
        name: 'content',
        type: 'text'
      });
      alertInputMessage.addButton({
        text: 'Annuler',
        role: 'cancel'
      });
      alertInputMessage.addButton({
        text: "Ajouter",
        handler: data => {
          this.storeContactMessageIntoCache(contact, data.content);
          alertInputMessage.dismiss();
          return false;
        }
      });

      alertInputMessage.present();

    })
    .catch(err => {
      console.error("Error while picking the contact : ", err);
    });
  }

  displayEditMessageAlert(indexContactMessage : number){
    let isDefaultMessage = indexContactMessage == -1;
    let alertMessage = 
      isDefaultMessage ?
      "Edite le message par défaut" :
      "Edite le message pour " + this.contactMessages[indexContactMessage].contact.name;

    let currentMessage =
      isDefaultMessage ?
      this.defaultMessage :
      this.contactMessages[indexContactMessage].message

    let alertOptions = {
      title : "Editer",
      message : alertMessage
    };
    let alertInputMessage = this.alertCtrl.create(alertOptions);
    alertInputMessage.addInput({
      name: 'content',
      type: 'text',
      value : currentMessage
    });
    alertInputMessage.addButton({
      text: 'Annuler',
      role: 'cancel'
    });
    alertInputMessage.addButton({
      text: "Ajouter",
      handler: data => {
        this.updateContactMessageInCache(indexContactMessage, data.content);
        alertInputMessage.dismiss();
        return false;
      }
    });
    alertInputMessage.present();
  }


}
