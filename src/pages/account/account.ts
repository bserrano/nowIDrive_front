import { Component, OnInit } from '@angular/core';
import { AlertController, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

//Constants
import { GLOBAL } from '../../constants/global.ts';
@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage implements OnInit {

  protected user : any;

  constructor(
    private alertCtrl : AlertController,
    private navCtrl: NavController,
    private storage : Storage
  ){
    this.user = {
      mail: null,
      insurance : null
    }
  }

  ngOnInit(){
    this.getUserInfoFromCache();
  }

  //------------------------------------------------------------------//
  //----------------------------CACHE---------------------------------//
  //------------------------------------------------------------------//

  getUserInfoFromCache(){
    this.storage.get(GLOBAL.appName+"_user").then(user => {
      if(user == null){
        this.user = {
          mail: null,
          insurance : null
        }
      }else{
        this.user = user;
      }
    })
  }

  storeUserInfoIntoCache(userMail = "", userInsurance = ""){
    if(userMail !== ""){
      this.user.mail = userMail;
    }
    if(userInsurance !== ""){
      this.user.insurance = userInsurance;
    }
    this.storage.set(GLOBAL.appName+"_user", this.user);
  }

  //------------------------------------------------------------------//
  //-----------------------MODAL & ALERT------------------------------//
  //------------------------------------------------------------------//

  displayEditMailAlert(){
    let alertEditMail = this.alertCtrl.create();
    alertEditMail.setTitle("Mail");
    alertEditMail.setMessage("Edite ici ton adresse e-mail :");
    alertEditMail.addInput({
      name: 'mail',
      type: 'text',
      value : this.user.mail
    });
    alertEditMail.addButton({
      text: 'Annuler',
      role: 'cancel'
    });
    alertEditMail.addButton({
      text: "Editer",
      handler: data => {
        this.storeUserInfoIntoCache(data.mail, "");
        alertEditMail.dismiss();
        return false;
      }
    });
    alertEditMail.present();
  }

  displayEditInsuranceAlert(){
    let alertEditInsurance = this.alertCtrl.create();
    alertEditInsurance.setTitle("Assurance");
    alertEditInsurance.setMessage("Edite ici le nom de ton assurance :");
    alertEditInsurance.addInput({
      name: 'insurance',
      type: 'text',
      value : this.user.insurance
    });
    alertEditInsurance.addButton({
      text: 'Annuler',
      role: 'cancel'
    });
    alertEditInsurance.addButton({
      text: "Editer",
      handler: data => {
        this.storeUserInfoIntoCache("", data.insurance);
        alertEditInsurance.dismiss();
        return false;
      }
    });
    alertEditInsurance.present();
  }
}
