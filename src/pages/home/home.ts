import { Component, OnInit } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

//Constants
import { GLOBAL } from '../../constants/global';

//Modals
import { JourneyModal } from '../../modals/journey/journey';

//Plugins
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{
  protected distanceScore : number;

  constructor(
    private modalCtrl : ModalController,
    private socialSharing : SocialSharing,
    private storage : Storage
  ) {
    this.distanceScore = 0;
  }

  ngOnInit(){
    this.getDistanceScore();
  }

  startJourney(){
    let modalStartJourney = this.modalCtrl.create(JourneyModal);
    modalStartJourney.present();
  }

  shareScore(){
    this.socialSharing.share(
      "J'ai parcouru "+this.distanceScore+" kilomètres sans regarder mon téléphone, en utilisant Now I Drive !" , //Message
      "Now I Drive", //Subject
      null,
      null
    );
  }

  getDistanceScore(){
    this.storage.get(GLOBAL.appName+"_distanceScore").then(distanceScore => {
      if(distanceScore == null){
        console.warn("No distance score registered in the cache");
      }else {
        this.distanceScore = distanceScore;
        console.log("Current distance score : ", this.distanceScore);
      }
    });
  }

}
