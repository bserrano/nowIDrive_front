import { Injectable, Pipe } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'niceTime'
})

@Injectable()
export class NiceTime {

  transform(value: string, args: any[]) {
    let date = new Date(parseInt(value));

    return moment(date).format("hh:mm:ss");
  }

}