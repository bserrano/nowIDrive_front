import { Injectable, Pipe } from '@angular/core';

@Pipe({
  name: 'niceNumber'
})
@Injectable()
export class NiceNumber {
  transform(value: string, args: any) {
    if (value != null) {
      let parsedValue = parseInt(value);
      switch(args){
        case "milliseconds-time":
          let totalSeconds = Math.floor(parsedValue / 1000);
          let hours = Math.floor(totalSeconds / 3600);
          let minutes = Math.floor((totalSeconds - (hours*3600)) / 60);
          let seconds = totalSeconds - (hours * 3600) - (minutes * 60);
          let text =
            (hours < 10 ? "0" + hours : hours.toString()) + ":" +
            (minutes < 10 ? "0" + minutes : minutes.toString()) + ":" +
            (seconds < 10 ? "0" + seconds : seconds.toString());
          return text;
        default:
          return parsedValue;
      }
    }
    return "";
  }
}
